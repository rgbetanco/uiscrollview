//
//  ViewController.swift
//  UIScrollView
//
//  Created by Ronald Garcia on 27/7/2017.
//  Copyright © 2017 com.rgbetanco.uiscrollview. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var scrollView: UIScrollView!
    
    var images = [UIImageView]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        let swidth: CGFloat = scrollView.frame.size.width
        let sheight: CGFloat = scrollView.frame.size.height
        
        var contentWidth: CGFloat = 0.0
        for x in 0...2 {
            let image = UIImage(named:"icon\(x).png")
            let imageVIew = UIImageView(image: image)
            images.append(imageVIew)
            
            var newX: CGFloat = 0.0
            //newX = view.frame.midX + view.frame.size.width * CGFloat(x)
            newX = swidth / 2 + swidth * CGFloat(x)
            contentWidth += newX
            scrollView.addSubview(imageVIew)
            //imageVIew.frame = CGRect(x: newX - 75, y: view.frame.size.height/2 - 75, width: 150, height: 150)
            imageVIew.frame = CGRect(x: newX - 75, y: sheight/2 - 75, width: 150, height: 150)
            scrollView.contentSize = CGSize(width: contentWidth, height: view.frame.size.height)
            scrollView.clipsToBounds = false
        }
        


    }

    

}

